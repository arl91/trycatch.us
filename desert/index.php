<?php

//Define constants that we use in the project
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS);
define('APP_PATH', ROOT . 'app' . DS );

//load the necesaries files
require_once APP_PATH . 'Config.php';
require_once APP_PATH . 'Request.php';
require_once APP_PATH . 'Bootstrap.php';
require_once APP_PATH . 'Controller.php';
require_once APP_PATH . 'View.php';

//run bootstrap
try{
	Bootstrap::run(new Request);
} catch(Exception $e){
	echo $e->getMessage();
}