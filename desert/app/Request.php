<?php

class Request
{
	private $_controlador;
	private $_metodo;
	private $_argumentos;

	public function __construct()
	{
		//read the uri and load the variables
		if(isset($_SERVER['REQUEST_URI'])){
			$url = explode('/', $_SERVER['REQUEST_URI']);
			$position = strpos($url[1], '?'); 
			if ($position) {
				$total = strlen($url[1]);
				$url[1] = substr($url[1], 0, -($total - $position)); 
			}
			$this->_controlador = strtolower($url[1]);
			if ($this->_controlador == 'edit' || $this->_controlador == 'delete' || $this->_controlador == '' || $this->_controlador == 'newrow') {
				$this->_controlador = 'address';
			}
			$this->_metodo = strtolower($url[1]);
			if ($this->_metodo == '') {
				$this->_metodo = 'address';
			}
			
			if (!empty($url[2])) {
				$this->_argumentos = array("id" => $url[2]);
			} else {
				$this->_argumentos = array();
			}
			
		}

		if(!isset($this->_argumentos)){
			$this->_argumentos = array();
		}
	}

	public function getControlador()
	{
		return $this->_controlador;
	}

	public function getMetodo()
	{
		return $this->_metodo;
	}

	public function getArgumentos()
	{
		return $this->_argumentos;
	}
}