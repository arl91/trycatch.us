<?php
/*
* SCRIPT A
* Author: Adrian Rubio Leon
*/

//initialize variables
$total = 1000;
$multiple1 = 3;
$multiple2 = 5;
$sum = 0;
$i = 1;

//calculate the sum multiples of 3
do {
	$res = $i * $multiple1;
	if ($res < $total) {
		$sum = $res + $sum;
	}
	$i++;
} while($res < $total);

$i = 1;
//calculate the sum multiples of 5
do {
	$res = $i * $multiple2;
	if ($res < $total) {
		$sum = $res + $sum;
	}
	$i++;
} while($res < $total);

//write the result
echo "The result is: $sum\n";