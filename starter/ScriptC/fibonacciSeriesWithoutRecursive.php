<?php
/*
* SCRIPT C
* Author: Adrian Rubio Leon
*/
echo "0, 1";
fibonacciSeries(0, 1, 0, 1);

function fibonacciSeries($start, $end, $total, $count) {
	for ($i = 1; $i < 9; $i++) {
		$total = $start + $end;
		$count++;
		echo ", $total";
		$start = $end;
		$end = $total;
	}
	echo "\nFunction terminated\n";
}