<?php
/*
* SCRIPT C
* Author: Adrian Rubio Leon
*/
echo "0, 1";
fibonacciSeries(0, 1, 0, 1);

function fibonacciSeries($start, $end, $total, $count) {
	if ($count < 9) {
		$total = $start + $end;
		$count++;
		echo ", $total";
		fibonacciSeries($end, $total, $total, $count);
	} else {
		echo "\nFunction terminated\n";
	}
}