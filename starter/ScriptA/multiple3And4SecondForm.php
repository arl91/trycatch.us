<?php
/*
* SCRIPT A
* Author: Adrian Rubio Leon
*/

$total = 1000;
$multiple1 = 3;
$multiple2 = 4;
$sum = 0;

for ($i = 1; $i < $total; $i++) {
	$res = $i * $multiple1;
	if ($res < $total) {
		$sum = $res + $sum;
	}
	$res = $i * $multiple2;
	if ($res < $total) {
		$sum = $res + $sum;
	}
}

echo "The result is: $sum\n";