<?php
/*
* SCRIPT B
* Author: Adrian Rubio Leon
*/

$result = calculatePower(2, 5);

echo "result is: $result\n";

/*
* calculate Power
* @param: $number - int number
* @param: $power - int number
*
* @return: result power or error
*/

function calculatePower($number, $power) {
	if (!empty($number) && !empty($power)) {
		$res = pow($number, $power);
	} else {
		$res = 'Error';
	}
	return $res;
}