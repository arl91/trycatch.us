<?php

abstract class Controller
{
    protected $_view;
 
    public function __construct()
    {
        $this->_view = new View(new Request);
    }
 
    abstract public function address($id);
    abstract public function edit($id);
    abstract public function delete($id);
    abstract public function newRow();
}