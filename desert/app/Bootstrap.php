<?php

class Bootstrap
{
/*
* Funcion load de application and load thhe routing system
*
*/
	public static function run(Request $peticion)
	{
		//obtaing the name of controller
		$controller = $peticion->getControlador() . 'Controller';
		//we construct the routo of controller
		$routeControlador = ROOT . 'controllers' . DS . $controller . '.php';

		//obtaing the name of the function
		$metodo = $peticion->getMetodo();
		//if we have arguments we load this
		$argumentos = $peticion->getArgumentos();
		//if the controller exist, we load this controller
		if(is_readable($routeControlador)){
			require_once $routeControlador;
			$controller = new $controller;
			if(is_callable(array($controller, $metodo))){
				$metodo = $peticion->getMetodo();
			} else {
				$metodo = 'index';
			}
			if(!empty($argumentos)){
				call_user_func_array(array($controller, $metodo), $argumentos);
			} else {
				call_user_func(array($controller, $metodo));
			}
			//if the controller don't exist we send message
		} else {
			throw new Exception('Page not found');
		}
	}
}