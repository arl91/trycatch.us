<?php

class View
{
	private $_controlador;

	public function __construct(Request $peticion)
	{
		$this->_controlador = $peticion->getControlador();
	}

	public function render($vista)
	{
		//if the view exist, we loaded
		$rutaView = ROOT . 'views' . DS . $this->_controlador . DS . $vista . '.phtml';
		if(is_readable($rutaView)){
			require_once $rutaView;
		} else {
			throw new Exception('Error load view');
		}
	}
}