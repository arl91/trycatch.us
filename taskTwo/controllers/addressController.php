<?php

class addressController extends Controller
{

	private $_addresses;
	public function __construct() {
		parent::__construct();
	}

	public function address($id = 0) {
		$this->rcd();
		echo json_encode($this->_addresses);
	}

	private function rcd() {
		//load the csv and save data in the variable
		$file = fopen(ROOT . 'public/example.csv', 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$this->_addresses[] = array(
				'name' => $line[0],
				'phone' => $line[1],
				'street' => $line[2]
			);
		}

		fclose($file);
	}
}